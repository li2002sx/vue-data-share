// 懒加载
const Index = r => require.ensure([], () => r(require('../frames/Index.vue')), 'Index')
const CateIndex = r => require.ensure([], () => r(require('../frames/cate/Index.vue')), 'CateIndex')
const UserIndex = r => require.ensure([], () => r(require('../frames/user/Index.vue')), 'UserIndex')
const FindPass = r => require.ensure([], () => r(require('../frames/user/FindPass.vue')), 'FindPass')
const ArtiList = r => require.ensure([], () => r(require('../frames/cate/Arti.vue')), 'ArtiList')
const Search = r => require.ensure([], () => r(require('../frames/cate/Search.vue')), 'Search')
const Feedback = r => require.ensure([], () => r(require('../frames/user/Feedback.vue')), 'Feedback')
const ArtiDetail = r => require.ensure([], () => r(require('../frames/cate/Detail.vue')), 'ArtiDetail')
const ArtiVideo = r => require.ensure([], () => r(require('../frames/cate/Video.vue')), 'ArtiVideo')
const Login = r => require.ensure([], () => r(require('../frames/user/Login.vue')), 'Login')
const MyArtiList = r => require.ensure([], () => r(require('../frames/user/Arti.vue')), 'MyArtiList')
export default [{
  path: '',
  redirect: '/index'
}, {
  path: '/index',
  component: Index
}, {
  path: '/cate',
  component: CateIndex
}, {
  path: '/user',
  component: UserIndex
}, {
  path: '/user/findpass',
  component: FindPass
}, {
  path: '/cate/:cateId/arti',
  component: ArtiList
}, {
  path: '/arti/searchlist/:searchKey',
  component: ArtiList
}, {
  path: '/cate/search',
  component: Search
}, {
  path: '/user/feedback',
  component: Feedback
}, {
  path: '/arti/detail/:artiId',
  component: ArtiDetail
}, {
  path: '/arti/video/:artiId',
  component: ArtiVideo
}, {
  path: '/login',
  component: Login
}, {
  path: '/user/:relatedType/arti',
  component: MyArtiList
}]
