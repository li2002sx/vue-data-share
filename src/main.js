// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import App from './App'
import VueRouter from 'vue-router'
import routes from './router/router'
import store from './vuex/store'
import mUtils from './config/mUtils'
import axios from '../node_modules/axios'
import './config/globalUtil'
import Qs from 'qs'
// 引入echarts
// import echarts from 'echarts'

// Vue.use(viewPort)

// import pdfjs from 'pdfjs'

// plugins
import {
  ToastPlugin,
  AlertPlugin,
  ConfirmPlugin,
  LoadingPlugin,
  WechatPlugin,
  DatetimePlugin,
  AjaxPlugin
} from 'vux'

Vue.use(VueRouter)
Vue.use(mUtils)
Vue.use(ToastPlugin)
Vue.use(AlertPlugin)
Vue.use(ConfirmPlugin)
Vue.use(LoadingPlugin)
Vue.use(WechatPlugin)
Vue.use(DatetimePlugin)
Vue.use(AjaxPlugin)

// Vue.prototype.$pdfjs = pdfjs

const router = new VueRouter({
  routes
})

// 点击延迟
FastClick.attach(document.body)

// 日志输出开关
Vue.config.productionTip = false

const baseURL = global.apiUrl
const timeOut = 20000

Vue.prototype.getFieldByUseInfo = function (field) {
  let userInfo = window.localStorage.getItem(global.userInfo)
  if (userInfo !== null) {
    userInfo = JSON.parse(userInfo)
    if (userInfo !== null) {
      return Reflect.get(userInfo, field) || ''
    }
  }
  return ''
}

Vue.prototype.isLogin = function () {
  if (this.getFieldByUseInfo('token') === '') {
    return false
  } else {
    return true
  }
}

// axiosGet请求
Vue.prototype.get = function (url, params, callback, showLoading) {
  if (showLoading === undefined || showLoading === true) {
    // 显示
    this.$vux.loading.show({
      text: '加载中...'
    })
  }
  axios({
    url: url,
    method: 'get',
    baseURL: baseURL,
    headers: {
      'token': this.getFieldByUseInfo('token')
    },
    params: params,
    withCredentials: true, // default
    responseType: 'json', // default
    timeout: timeOut
  }).then(response => {
    // 隐藏
    this.$vux.loading.hide()
    let data = response.data
    if (data === null || data.status === -1) {
      this.toUrl('/login')
    } else {
      callback(data)
    }
  }).catch(error => {
    // 隐藏
    this.$vux.loading.hide()
    console.log(error)
  })
}

// axiosPost请求
Vue.prototype.post = function (url, data, callback, showLoad) {
  if (showLoad === true) {
    // 显示
    this.$vux.loading.show({
      text: '加载中...'
    })
  }
  axios({
    url: url,
    method: 'post',
    baseURL: baseURL,
    headers: {
      'token': this.getFieldByUseInfo('token'),
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      // 为了避免qs格式化时对内层对象的格式化先把内层的对象转为
      data.CustData = JSON.stringify(data.CustData)
      // 由于使用的form-data传数据所以要格式化
      data = Qs.stringify(data)
      return data
    }],
    transformResponse: [function (data) {
      return data
    }],
    data: data,
    withCredentials: true, // default
    responseType: 'json', // default
    timeout: timeOut,
    // `onUploadProgress`上传进度事件
    onUploadProgress: function (progressEvent) {
    },
    // 下载进度的事件
    onDownloadProgress: function (progressEvent) {
    }
  }).then(response => {
    let data = response.data
    if (data === null || data.status === -1) {
      this.toUrl('/login')
    } else {
      callback(data)
    }
    // 隐藏
    this.$vux.loading.hide()
  }).catch(error => {
    // 隐藏
    this.$vux.loading.hide()
    console.log(error)
  })
}

// axiosPost请求
Vue.prototype.postForm = function (url, data, callback) {
  // 显示
  this.$vux.loading.show({
    text: '加载中...'
  })
  axios({
    url: url,
    method: 'post',
    baseURL: baseURL,
    headers: {
      'token': this.getFieldByUseInfo('token'),
      'Content-Type': 'multipart/form-data'
    },
    transformRequest: [function (data) {
      // 为了避免qs格式化时对内层对象的格式化先把内层的对象转为
      // data.CustData = JSON.stringify(data.CustData)
      // 由于使用的form-data传数据所以要格式化
      // data = Qs.stringify(data)
      return data
    }],
    transformResponse: [function (data) {
      return data
    }],
    data: data,
    withCredentials: true, // default
    responseType: 'json', // default
    timeout: timeOut,
    // `onUploadProgress`上传进度事件
    onUploadProgress: function (progressEvent) {
    },
    // 下载进度的事件
    onDownloadProgress: function (progressEvent) {
    }
  }).then(response => {
    let data = response.data
    if (data === null || data.status === -1) {
      this.toUrl('/login')
    } else {
      callback(data)
    }
    // 隐藏
    this.$vux.loading.hide()
  }).catch(error => {
    // 隐藏
    this.$vux.loading.hide()
    console.log(error)
  })
}

Vue.prototype.toUrl = function (url) {
  if (url === '/login') {
    var referrer = document.URL
    if (referrer.indexOf('/login') === -1) {
      this.setStore('referrer', referrer.split('#')[1])
    } else {
      this.removeStore('referrer')
    }
  }
  if (url === -1) {
    window.history.go(-1)
  } else {
    // if (url.indexOf('/arti/detail') > -1) {
    //   location.href = '?#' + url
    // } else {
    //   this.$router.push(url)
    // }
    this.$router.push(url)
  }
}

Vue.prototype.loginToUrl = function () {
  var referrer = this.getStore('referrer')
  if (referrer === null) {
    referrer = '/index'
  }
  this.$router.push(referrer)
}

Vue.directive('title', {
  inserted: function (el, binding) {
    document.title = el.dataset.title
  }
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app-box')
